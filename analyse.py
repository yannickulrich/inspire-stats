import json
import time
import glob


def parse_affiliation(a):
    extra_affiliations = {
        'CCAST World Lab, Beijing': 905335,
        'Peking U., SKLNPT': 1210798,
        'Peking U.': 903603,
        'Lyon, IPN': 902974,
        'CICQM, Beijing': 1436897,
        'Peking U., CHEP': 911671,
        'Nankai U.': 906082,
        'Tianjin U.': 1604854,
        'Southern Denmark U., CP3-Origins': 911713,
        '. Southern Denmark, Odense, DIAS': 1263319,
        'IAS, Julich': 911932,
        'JCHP, Julich': 911539,
        'Frankfurt U.': 904315,
        'Bonn U., HISKP': 908572,
        'U. Bonn, Phys. Inst., BCTP': 1471435,
        'Riphah Int.U., Islamabad': 1118469,  # no really but close enough
        'CERN': 902725
    }

    if 'record' in a[1]:
        return int(a[1]['record']['$ref'].split('/')[-1])
    elif a[1]['value'] in extra_affiliations:
        return extra_affiliations[a[1]['value']]
    else:
        print(a[1]['value'])


def add_ts(papers):
    for p in papers:
        p['ts'] = int(time.mktime(
            time.strptime(p['legacy_date'], '%Y-%m-%d')
        ))
        yield p


def load_papers():
    all_papers = []
    for fn in sorted(glob.glob('dat/filtered-hep-ph-????.json')):
        with open(fn) as fp:
            all_papers += json.load(fp)

    all_affiliations = set(
        parse_affiliation(a)
        for p in all_papers
        for a in p['affiliations']
    )
    return list(add_ts(all_papers)), all_affiliations


def chop_bin(papers, start, step):
    if type(start) == str:
        start = int(time.mktime(time.strptime(start, '%Y-%m-%d')))

    while len(papers) > 0:
        end = start + step
        yield (start, [
            i
            for i in papers
            if start <= i['ts'] < end
        ])
        papers = [
            i
            for i in papers
            if not (i['ts'] < end)
        ]
        start += step


def count_authors(papers):
    lst = dict((i, set()) for i in all_affiliations)
    for p in papers:
        for a in p['affiliations']:
            if a[0]['recid']:
                lst[parse_affiliation(a)].add(a[0]['recid'])
            else:
                lst[parse_affiliation(a)].add(a[0]['bai'])

    return sorted([
        (aff, len(auths))
        for aff, auths in lst.items()
    ], key=lambda a: a[1])


def bypostdoc(papers):
    lst = dict((i, []) for i in all_affiliations)
    for ts, ps in chop_bin(papers, '1992-10-01', 3600*24*365.25):
        for a, n in count_authors(ps):
            lst[a].append([ts, n])
    return {
        a: np.array(i) for a, i in lst.items()
    }
