import requests
import json
import time


def fetch(url):
    ans = []
    while True:
        print("Fetching "+url)
        j = requests.get(url).json()
        ans += j['hits']['hits']

        if 'next' in j['links']:
            url = j['links']['next']
        else:
            break

    return ans


def download1(primarch, year):
    url = (
        "https://inspirehep.net/api/literature"
        "?sort=mostrecent&size=%d&page=%d&q="
        "primarch%%20%s%%20and%%20dadd%%20%d"
    ) % (
        1000, 1, primarch, year
    )
    return fetch(url)


def filter_meta(papers):
    for paper in papers:
        try:
            aff = [
                (
                    {
                        'recid': (
                            i['recid'] if 'recid' in i else None
                        ),
                        'bai': (
                            i['bai'] if 'bai' in i else None
                        )
                    },
                    j
                )
                for i in paper['metadata']['authors']
                if 'affiliations' in i
                for j in i['affiliations']
            ]
            yield {
                'citations': paper['metadata']['citation_count'],
                'affiliations': aff,
                'legacy_date': (
                    paper['metadata']['legacy_creation_date']
                    if 'legacy_creation_date' in paper['metadata']
                    else
                    None
                ),
                'date': (
                    paper['metadata']['earliest_date']
                    if 'earliest_date' in paper['metadata']
                    else
                    None
                ),
                'keywords': (
                    paper['metadata']['keywords']
                    if 'keywords' in paper['metadata']
                    else
                    None
                )
            }
        except KeyError:
            print("Error in id " + paper['id'])


def download(primarch, y0, y1, timeout=5):
    t0 = time.time()
    all_papers = []
    for y in range(y0, y1+1):
        time.sleep(timeout)
        print("Downloading year %d" % y)
        papers = download1(primarch, y)
        with open('dat/raw-%s-%d.json' % (primarch, y), 'w') as fp:
            json.dump(papers, fp)
        f = list(filter_meta(papers))
        with open('dat/filtered-%s-%d.json' % (primarch, y), 'w') as fp:
            json.dump(f, fp)
        all_papers += f

    with open('dat/filtered-%s.json' % primarch, 'w') as fp:
        json.dump(all_papers, fp)
    t1 = time.time()

    print("Downloading took ", t1-t0)
    return all_papers


def filter_inst(insts):
    for inst in insts:
        try:
            yield (
                int(inst['id']),
                {
                    'name': inst['metadata']['legacy_ICN'],
                    'id': int(inst['id']),
                    'coord':  [
                        (i['latitude'], i['longitude'])
                        for i in inst['metadata']['addresses']
                        if 'latitude' in i
                    ]
                }
            )
        except KeyError:
            pass


def download_institutions(timeout=5):
    url = (
        "https://inspirehep.net/api/institutions/"
        "?size=1000&page=1&q=control_number%%3A[%d TO %d]"
    )
    insts = fetch(url % (1, 1000000))
    time.sleep(timeout)
    insts += fetch(url % (1000001, 2000000))
    with open('dat/raw-inst.json', 'w') as fp:
        json.dump(insts, fp)
    f = dict(filter_inst(insts))
    with open('dat/filtered-inst.json', 'w') as fp:
        json.dump(f, fp)
    return f


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Downloading INSPIRE archive')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--literature', action='store_true')
    group.add_argument('--institutions', action='store_false')

    parser.add_argument(
        '-s', type=int, help='Start year', default=2000
    )
    parser.add_argument(
        '-f', type=int, help='Finish year', default=2021
    )
    parser.add_argument(
        '-a', type=str, help='Primary arxiv', default='hep-ph'
    )
    parser.add_argument(
        '-t', type=float, help='Timeout', default=5
    )
    args = parser.parse_args()

    if args.literature:
        download(args.a, args.s, args.f, timeout=args.t)
    else:
        download_institutions(timeout=args.t)
