# INSPIREhep statistics

It is an oft-repeated factoid the [IPPP](https://ippp.dur.ac.uk) is
the 'largest' phenomenology group in $`X`$ where $`X`$ varies
depending on source, circumstance etc. Of course, the exact meaning of
*largest* (or phenomenology for that matter) is usually not specified.
I would consider the following options:

 * most papers
 * most citations
 * most authors
 * largest budget
 * ...

The first three options can of course be easily quantified by looking
at bibliometrics on [INSPIRE](https://inspirehep.net). This is what I
attempt here

 1. Download relevant dataset. This is no longer (?) possible in bulk.
    Instead we have to repeatedly poll the [INSPIRE
    API](https://github.com/inspirehep/rest-api-doc).
    [Pagination](https://github.com/inspirehep/rest-api-doc#pagination),
    [maximum number of records](https://github.com/inspirehep/rest-api-doc/issues/20#issuecomment-997932079),
    and [rate limiting](https://github.com/inspirehep/rest-api-doc#rate-limiting)
    makes this a bit challenging.

 2. Reduce the dataset by collecting the author name and affiliations
    for each paper.

 3. Bin and visualise the results.

## Download
Looking at a single `primarch` such as `hep-ph` there are usually
$`\mathcal{O}(5{\rm k})`$ papers per year. With a pagination limit of
1000 results per page, we have need roughly 5 requests per year.

Since I want to look at least at the last 20-30 years, I need roughly
150 requests for `hep-ph`. Unfortunately, the INSPIRE API is rather
slow in responding so that even one request takes $`\mathcal{O}(30{\rm
s})`$.
